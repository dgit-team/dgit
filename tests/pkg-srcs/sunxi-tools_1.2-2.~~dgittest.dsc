Format: 3.0 (quilt)
Source: sunxi-tools
Binary: sunxi-tools
Architecture: any
Version: 1.2-2.~~dgittest
Maintainer: Ian Campbell <ijc@hellion.org.uk>
Homepage: http://linux-sunxi.org/Sunxi-tools
Standards-Version: 3.9.5
Vcs-Browser: http://git.debian.org/?p=collab-maint/sunxi-tools.git
Vcs-Git: git://git.debian.org/collab-maint/sunxi-tools.git
Build-Depends: debhelper (>= 9), pkg-config, libusb-1.0-0-dev, u-boot-tools
Package-List:
 sunxi-tools deb utils optional arch=any
Checksums-Sha1:
 2457216dbbf5552c413753f7211f7be3db6aff54 35076 sunxi-tools_1.2.orig.tar.gz
 491322a7e377365cf53104b5dda6a30ede0c01f7 4892 sunxi-tools_1.2-2.~~dgittest.debian.tar.xz
Checksums-Sha256:
 03a63203ff79389e728d88ad705e546aa6362a6d08b9901392acb8639998ef95 35076 sunxi-tools_1.2.orig.tar.gz
 33b627e8958f1bc6d2a9bf1d1a042ac808924d860c09272989067fd57b9fb8e6 4892 sunxi-tools_1.2-2.~~dgittest.debian.tar.xz
Files:
 dbc55f60559f9db497559176c3c753dd 35076 sunxi-tools_1.2.orig.tar.gz
 528d6bb421ba55aa1cec176298f8f14c 4892 sunxi-tools_1.2-2.~~dgittest.debian.tar.xz
