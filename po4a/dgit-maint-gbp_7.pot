# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-04-11 22:39+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: =head1
#: ../dgit.1:3 ../dgit.7:2 ../dgit-user.7.pod:1 ../dgit-nmu-simple.7.pod:1
#: ../dgit-maint-native.7.pod:1 ../dgit-maint-merge.7.pod:1
#: ../dgit-maint-gbp.7.pod:1 ../dgit-maint-debrebase.7.pod:1
#: ../dgit-downstream-dsc.7.pod:1 ../dgit-sponsorship.7.pod:1
#: ../dgit-maint-bpo.7.pod:1 ../git-debrebase.1.pod:1 ../git-debrebase.5.pod:1
#: ../git-debpush.1.pod:1
#, no-wrap
msgid "NAME"
msgstr ""

#. type: =head1
#: ../dgit.1:1694 ../dgit.7:23 ../dgit-user.7.pod:447
#: ../dgit-nmu-simple.7.pod:137 ../dgit-maint-native.7.pod:125
#: ../dgit-maint-merge.7.pod:509 ../dgit-maint-gbp.7.pod:139
#: ../dgit-maint-debrebase.7.pod:792 ../dgit-downstream-dsc.7.pod:352
#: ../dgit-sponsorship.7.pod:325 ../dgit-maint-bpo.7.pod:140
#: ../git-debrebase.1.pod:643 ../git-debrebase.5.pod:678
#: ../git-debpush.1.pod:261
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:5 ../dgit-maint-native.7.pod:5
#: ../dgit-maint-merge.7.pod:5 ../dgit-maint-gbp.7.pod:5
#: ../dgit-maint-debrebase.7.pod:5 ../dgit-downstream-dsc.7.pod:5
#: ../dgit-maint-bpo.7.pod:5 ../git-debrebase.5.pod:5
msgid "INTRODUCTION"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:222 ../dgit-maint-gbp.7.pod:56
msgid "BUILDING"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:449 ../dgit-maint-native.7.pod:127
#: ../dgit-maint-gbp.7.pod:141
msgid "dgit(1), dgit(7)"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:37 ../dgit-maint-gbp.7.pod:16
#: ../dgit-maint-debrebase.7.pod:38
msgid ""
"Benefit from dgit's safety catches.  In particular, ensure that your upload "
"always matches exactly your git HEAD."
msgstr ""

#. type: =head1
#: ../dgit-maint-merge.7.pod:498 ../dgit-maint-gbp.7.pod:133
#: ../dgit-maint-debrebase.7.pod:596
msgid "INCORPORATING NMUS"
msgstr ""

#. type: =head1
#: ../dgit-maint-merge.7.pod:513 ../dgit-maint-gbp.7.pod:143
#: ../dgit-maint-debrebase.7.pod:796 ../dgit-maint-bpo.7.pod:144
#: ../git-debpush.1.pod:266
msgid "AUTHOR"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:3
msgid ""
"dgit - tutorial for package maintainers already using git-buildpackage(1)"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:7
msgid ""
"This document explains how B<dgit> can be incorporated into a git-"
"buildpackage(1) package-maintenance workflow.  This should be read jointly "
"with git-buildpackage(1)'s documentation.  Some reasons why you might want "
"to incorporate B<dgit> into your existing workflow:"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:21
msgid ""
"Provide a better, more detailed git history to downstream dgit users, such "
"as people using dgit to do an NMU (see dgit-nmu-simple(7) and dgit-user(7))."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:27
msgid ""
"Note that we assume a patches-unapplied repository: the upstream source "
"committed to the git repository is unpatched.  git-buildpackage(1) can work "
"with patched-applied repositories, but is normally used with patches-"
"unapplied."
msgstr ""

#. type: =head1
#: ../dgit-maint-gbp.7.pod:32 ../dgit-maint-debrebase.7.pod:334
msgid "GIT CONFIGURATION"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:34
msgid ""
"If you have configured an I<export-dir> in your gbp.conf, you should tell "
"dgit about it:"
msgstr ""

#. type: verbatim
#: ../dgit-maint-gbp.7.pod:39
#, no-wrap
msgid ""
"    % git config --global dgit.default.build-products-dir /home/spwhitton/build-area\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:43
msgid "If you run"
msgstr ""

#. type: verbatim
#: ../dgit-maint-gbp.7.pod:47
#, no-wrap
msgid ""
"    % git config dgit.default.quilt-mode gbp\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:51
msgid "in your repository, you can omit I<--gbp> wherever it occurs below."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:53
msgid ""
"Note that this does require that you always work from your gbp master "
"branch, never the dgit patches-applied branch."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:58
msgid "You can perform test builds like this:"
msgstr ""

#. type: verbatim
#: ../dgit-maint-gbp.7.pod:62
#, no-wrap
msgid ""
"    % dgit [--include-dirty] gbp-build [OPTIONS]\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:66
msgid ""
"where I<--include-dirty> is needed for testing uncommitted changes, and "
"I<OPTIONS> are any further options to be passed on to gbp-buildpackage(1)."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:70
msgid ""
"If you are doing a source-only upload, you do not need to prepare a "
"I<_source.changes>, as B<dgit push-source> will take of that on your behalf."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:74
msgid ""
"If you need to include binaries with your upload, you will probably want to "
"use sbuild(1), pbuilder(1) or cowbuilder(1):"
msgstr ""

#. type: verbatim
#: ../dgit-maint-gbp.7.pod:79
#, no-wrap
msgid ""
"    % dgit --rm-old-changes --gbp sbuild\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:83
msgid "replacing 'sbuild' with 'pbuilder' or 'cowbuilder' if appropriate."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:85
msgid ""
"We use I<--rm-old-changes> to ensure that there is exactly one changes file "
"corresponding to this package, so we can be confident we're uploading what "
"we intend (though B<dgit push> will do some safety checks)."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:90
msgid ""
"Note that none of the commands in this section are required to upload with "
"dgit.  You can invoke gbp-buildpackage(1), pbuilder(1), cowbuilder(1) and "
"sbuild(1) directly.  However, the defaults for these tools may leave you "
"with something that dgit will refuse to upload because it doesn't match your "
"git HEAD."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:96
msgid "As a general rule, leave all signing and tagging to dgit."
msgstr ""

#. type: =head1
#: ../dgit-maint-gbp.7.pod:98
msgid "UPLOADING"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:100
msgid ""
"Don't use I<--git-tag>: B<dgit push> will do this for you.  To do a source-"
"only upload:"
msgstr ""

#. type: verbatim
#: ../dgit-maint-gbp.7.pod:105 ../dgit-sponsorship.7.pod:176
#, no-wrap
msgid ""
"    % dgit --gbp push-source\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:109
msgid "or if you need to include binaries,"
msgstr ""

#. type: verbatim
#: ../dgit-maint-gbp.7.pod:113
#, no-wrap
msgid ""
"    % dgit --gbp push\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:117
msgid ""
"This will push your git history to the dgit-repos, but you probably want to "
"follow it up with a push to salsa."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:120
msgid ""
"You will need to pass I<--trust-changelog> if the previous upload was not "
"performed with dgit."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:123
msgid ""
"If this is first ever dgit push of the package, consider passing I<--"
"deliberately-not-fast-forward> instead of I<--trust-changelog>.  This avoids "
"introducing a new origin commit into the dgit view of your git history.  "
"(This origin commit would represent the most recent non-dgit upload of the "
"package, but this should already be represented in your git history.)"
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:130
msgid ""
"Alternatively, you can use git-debpush(1).  For the first upload you should "
"pass the B<--gbp> quilt mode option (see git-debpush(1))."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:135
msgid ""
"B<dgit pull> can't yet incorporate NMUs into patches-unapplied gbp "
"branches.  You can just apply the NMU diff the traditional way.  The next "
"upload will require I<--trust-changelog>."
msgstr ""

#. type: textblock
#: ../dgit-maint-gbp.7.pod:145
msgid ""
"This tutorial was written and is maintained by Sean Whitton "
"<spwhitton@spwhitton.name>."
msgstr ""
